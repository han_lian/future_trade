#pragma once

#include <string>
#include <stdint.h>
#include <functional>
#include <unordered_map>

using std::unordered_map;

using RequestHandler = std::function<void(struct mg_connection* conn, void*, void*, std::string)>;

class WdRouter
{
public:
	WdRouter();
	~WdRouter();

	RequestHandler GetRequestHandlerByUrl(const std::string& url);

	void Get(const std::string& url, RequestHandler handler);

	void Post(const std::string& url, RequestHandler handler);

	void Delete(const std::string& url, RequestHandler handler);

	void Put(const std::string& url, RequestHandler handler);

private:
	std::unordered_map<std::string, RequestHandler> url_handler_map_;

};


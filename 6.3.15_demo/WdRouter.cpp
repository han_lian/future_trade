#include "stdafx.h"
#include "WdRouter.h"
#include "mongoose.h"

void http404(struct mg_connection* conn, void* request, void* func, std::string response)
{
	mg_http_reply(conn, 404, "", "Not Found");
}

WdRouter::WdRouter()
{

}

WdRouter::~WdRouter()
{

}

RequestHandler WdRouter::GetRequestHandlerByUrl(const std::string& url)
{
	auto iter = url_handler_map_.find(url);
	if (iter != url_handler_map_.end())
		return iter->second;
	else
		return http404;
}

void WdRouter::Get(const std::string& url, RequestHandler handler)
{
	auto iter = url_handler_map_.find(url);
	if (iter == url_handler_map_.end())
		url_handler_map_[url] = handler;
}

void WdRouter::Post(const std::string& url, RequestHandler handler)
{
	auto iter = url_handler_map_.find(url);
	if (iter == url_handler_map_.end())
		url_handler_map_[url] = handler;
}

void WdRouter::Delete(const std::string& url, RequestHandler handler)
{
	auto iter = url_handler_map_.find(url);
	if (iter == url_handler_map_.end())
		url_handler_map_[url] = handler;
}

void WdRouter::Put(const std::string& url, RequestHandler handler)
{
	auto iter = url_handler_map_.find(url);
	if (iter == url_handler_map_.end())
		url_handler_map_[url] = handler;
}
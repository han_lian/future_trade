#pragma once

#include <string>
#include <stdint.h>
#include <future>
#include <functional>
#include "WdRouter.h"
#include "mongoose.h"

class WdHttpServer
{
public:
	WdHttpServer();
	WdHttpServer(WdHttpServer& other);
	~WdHttpServer();

	WdHttpServer bind(const std::string& addr);

	WdHttpServer router(WdRouter* router);

	int32_t run();

	static void RequestHandlerCallback(struct mg_connection* c, int ev, void* ev_data, void* fn_data);


public:
	static WdRouter* g_router;

protected:
	int32_t MongooseCallback();
	//void RequestHandler(struct mg_connection *c, int ev, void *ev_data, void *fn_data);


private:
	bool stop_;
	struct mg_mgr mgr_;
	std::future<int32_t> listen_future_;

};
